package polinema.mi.utspj_wicary

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_konfirmasi.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

const val Topics = "/topics/appx0f"

class konfimasi : AppCompatActivity(), View.OnClickListener {
    val Tag = "MainActivity"
    val RC_OK = 100
    var alamat = ""
    var pesanJarkom = ""
    var linkgbr = ""

        override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_konfirmasi)

        btnSend.setOnClickListener(this)
        btn_ubah.setOnClickListener(this)

        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
//            token.setText(it.token)
        }
        FirebaseMessaging.getInstance().subscribeToTopic(Topics)

        konf_pesanJarkom.setText(getIntent().getStringExtra("pesanJarkom"));
        konf_alamat.setText(getIntent().getStringExtra("alamat"));
        konf_gbr.setText(getIntent().getStringExtra("linkGbr"));

    }


    override fun onClick(v: View?) {
        val intent = Intent()
        intent.action = Intent.ACTION_GET_CONTENT
        when(v?.id){

            R.id.btnSend ->{
            alamat = konf_alamat.text.toString()
            pesanJarkom = konf_pesanJarkom.text.toString()
            linkgbr = konf_gbr.text.toString()
            if (alamat.isNotEmpty() && pesanJarkom.isNotEmpty()){
                pushNotifikasi(
                    Topics,
                    notifikasiData(alamat,linkgbr,pesanJarkom)
                ).also {
                    sendNotifikasi(it)
                }
            }
            Toast.makeText(this,"Berhasil Kirim Data",Toast.LENGTH_SHORT).show()
            }

            R.id.btn_ubah -> {
                val intent = Intent(this@konfimasi, MainActivity::class.java)
                intent.putExtra("pesanJarkom", pesanJarkom)
                intent.putExtra("alamat", alamat)
                intent.putExtra("linkGbr", linkgbr)
                startActivity(intent)
            }
        }
        if(v?.id == R.id.btnUpImg ) startActivityForResult(intent,RC_OK) //untuk bisa membuka explorer di hp
    }

    private  fun sendNotifikasi(notifikasi: pushNotifikasi) = CoroutineScope(Dispatchers.IO).launch {
        try {
            val response = RetrofitInstance.api.posNotif(notifikasi)
            if (response.isSuccessful){
                Log.d(Tag, "Response: ${Gson().toJson(response)}")
            }else{
                Log.e(Tag, response.errorBody().toString())
            }
        } catch (e :Exception){
            Log.e(Tag,e.toString())
        }
    }
}
