package polinema.mi.utspj_wicary

import polinema.mi.utspj_wicary.Constan.Companion.BASE_URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitInstance {

    companion object {
        private val retrovit by lazy {
            Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
        val api by lazy {
            retrovit.create(NotifikasiAPI::class.java)
        }
    }
}