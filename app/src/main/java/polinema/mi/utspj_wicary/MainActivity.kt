package polinema.mi.utspj_wicary

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


//const val Topic = "/topics/appx0f"

class MainActivity : AppCompatActivity() {
    val Tag = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        pesanJarkom.requestFocus()
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
        }

            pesanJarkom.setText(getIntent().getStringExtra("pesanJarkom"));
            alamat.setText(getIntent().getStringExtra("alamat"));


        Kirim.setOnClickListener {
            val intent = Intent(this@MainActivity, gambarActivity::class.java)
            intent.putExtra("pesanJarkom", pesanJarkom.getText().toString())
            intent.putExtra("alamat", alamat.getText().toString())
            startActivity(intent)
        }


    }

    private  fun sendNotifikasi(notifikasi: pushNotifikasi) = CoroutineScope(Dispatchers.IO).launch {
        try {
            val response = RetrofitInstance.api.posNotif(notifikasi)
            if (response.isSuccessful){
                Log.d(Tag, "Response: ${Gson().toJson(response)}")
            }else{
                Log.e(Tag, response.errorBody().toString())
            }
        } catch (e: Exception){
            Log.e(Tag, e.toString())
        }
    }
}
