package polinema.mi.utspj_wicary

import okhttp3.ResponseBody
import polinema.mi.utspj_wicary.Constan.Companion.Content_type
import polinema.mi.utspj_wicary.Constan.Companion.SERVER_KEY
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface NotifikasiAPI {
    @Headers("Authorization:key=$SERVER_KEY","Content-Type:$Content_type")
    @POST("/fcm/send")
    suspend fun posNotif(
        @Body notification : pushNotifikasi
    ): Response<ResponseBody>

}